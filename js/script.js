/*coded by Sandeep Rasali on 18 may, 2021 at 2:48am, darwin time*/
/* below coding attaches an event handler for animation on scroll*/

window.addEventListener("load", function(){
  AOS.init();
});







/*-------------------------toggle navbar----------------------------------*/ /*coded by Sandeep Rasali*/
const navToggler = document.querySelector(".nav-toggler");
navToggler.addEventListener("click", toggleNav);

function toggleNav(){
    navToggler.classList.toggle("active");
    document.querySelector(".nav").classList.toggle("open");
}

/*---------------------when clicking on the nav-item, it closes the navigation bar-------------------*/ /*coded by Sandeep Rasali*/

document.addEventListener("click", function(e){
    if(e.target.closest(".nav-item")){
        toggleNav();
    }
});

/*---------------------sticky header --------------------------*/ /*coded by Sandeep Rasali*/  /*additional feature in last MVP*/
window.addEventListener("scroll", function(){
    if(this.pageYOffset > 60){
        document.querySelector(".header").classList.add("sticky");
    }
    else{
        document.querySelector(".header").classList.remove("sticky");
    }
});

/*----------------------menu section---------------------------*/
/*This section will provide inaction to the user when they click on unactive category. */
/*This section is initialed by Nelson on 10th May 2021, last modified by Nelson on 12th May 2021.*/
const menuCate = document.querySelector(".menu-cate");
menuCate.addEventListener("click", function(e){
  if(e.target.classList.contains("menu-cate-item") && !e.target.classList.contains("active")){  /*if it is one of the listed class and it is not current;y active*/
    const target = e.target.getAttribute("data-target");
    menuCate.querySelector(".active").classList.remove("active");   /*remove active in current category */
    e.target.classList.add("active");  /*add active to selected category */
    /*display the selected category to the menu section */
    const menuSection = document.querySelector(".menu-section");
    menuSection.querySelector(".menu-cate-content.active").classList.remove("active"); /*remove active from current content */
    menuSection.querySelector(target).classList.add("active");    /*add active to selected content */
    AOS.init();    /*animation on scroll*/
   
  }
});

/*----------------------JS for Google Maps-------------------------*/

/*Coded by Sandeep Rasali (S342931) on May 10, 2021 at 2:35, Darwin time*/

/*This displays a marker at darwin of Australia where our domain called as "Amigos"
restaurant is located*/
/*When the user clicks the marker, an info window opens depicting some basic information 
about our restaurant */

function initMap() {
    const darwin = { lat: -12.4629, lng: 130.8449 };
    const map = new google.maps.Map(document.getElementById("map"), {
      zoom: 14,
      center: darwin,
    });
    const contentString = "<p><b>Amigos</b><br> The one and only Authentic Mexican Food in the town.</p>"
    const infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 200,
    });
    const marker = new google.maps.Marker({
      position: darwin,
      map,
      title: "Darwin (Amigos)",
    });
    marker.addListener("click", () => {
      infowindow.open(map, marker);
    });
  };


